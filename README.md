# Scrapy Project

## Run container

docker-compose run app scrapy

## Run spider

docker-compose run app scrapy list (list spiders)
docker-compose run app scrapy crawl \<spider\>

## Scrapy shell

### Examples for Scrapy Shell:

  > fetch('http://quotes.toscrape.com/')

* Extract text from all html 'a' tag:
  > response.xpath('//a/text()').extract()

* Extract text from all elements with class 'tag':
  > response.xpath('//*[@class="tag"]/text()').extract()

## XPATH

* [XPath Tester](https://www.freeformatter.com/xpath-tester.html)
* [XPath Helper (Chrome Plugin)](https://chrome.google.com/webstore/detail/xpath-helper/hgimnogjllphhhkhlmebbmlgjoejdpjl)
