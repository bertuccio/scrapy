FROM python:3.6-stretch

WORKDIR /usr/src/app

COPY requirements.file ./
RUN pip install --no-cache-dir -r requirements.file

COPY . .